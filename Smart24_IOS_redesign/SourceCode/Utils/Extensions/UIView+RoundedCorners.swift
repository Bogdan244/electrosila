//
//  UIView+RoundedCorners.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/27/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit
import QuartzCore

extension UIView {
    
    func roundedView(border: Bool, borderColor: UIColor?, borderWidth: CGFloat?, cornerRadius: CGFloat?) {
        
        if let radius = cornerRadius {
            layer.cornerRadius = radius
        } else {
            layer.cornerRadius = frame.size.width * 0.5
        }
        
        if border {
            
            if let borderColor = borderColor {
                layer.borderColor = borderColor.cgColor
            } else {
                layer.borderColor = UIColor.black.cgColor;
            }
            
            if let borderWidth = borderWidth {
                layer.borderWidth = borderWidth
            } else {
                layer.borderWidth = 0.0
            }
        }
        
        layer.masksToBounds = true
    }
}

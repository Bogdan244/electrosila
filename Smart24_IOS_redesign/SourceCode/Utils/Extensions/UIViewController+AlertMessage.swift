//
//  UIViewController+AlertMessage.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/17/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func showAlertMessage(title: String?, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    func showSmart24Error(error: ErrorEntity) {
        
        let alertController = UIAlertController(title: "", message: error.data, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default) { action in
            alertController.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(ok)
        present(alertController, animated: true, completion: nil)
    }
    
    func showSmart24Error(error: Error) {
        
        let alertController = UIAlertController(title: "Ошибка", message: error.localizedDescription, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default) { action in
            alertController.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(ok)
        present(alertController, animated: true, completion: nil)
    }
}

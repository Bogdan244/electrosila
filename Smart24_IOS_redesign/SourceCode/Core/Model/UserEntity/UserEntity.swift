//
//  UserEntity.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/18/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import Foundation
import ObjectMapper

class UserEntity: BaseEntity {
   
    dynamic var user_photo  = ""
    dynamic var telephone   = ""
    dynamic var name        = ""
    dynamic var username    = ""
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        user_photo      <- map[MapperKey.user_photo]
        telephone       <- map[MapperKey.telephone]
        name            <- map[MapperKey.name]
        username        <- map[MapperKey.username]
    }
    
    override class func objectForMapping(map: Map) -> BaseMappable? {
        return UserEntity()
    }
 
}

//
//  LoginViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/16/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import Foundation
import ReactiveCocoa
import ReactiveSwift
import Result
import Moya
import SwiftKeychainWrapper
import ObjectMapper
import enum Result.NoError
import FacebookCore
import FacebookLogin
import SwiftyVK
import RealmSwift
import MBProgressHUD

class LoginViewModel: GeneralViewModel {
    
    var cocoaActionLogin: CocoaAction<Any>!
    var loginSignalProducer: Action<(), UserEntity, ErrorEntity>!
    var errorSignalProducer: SignalProducer<(), NoError>!
    
    let userPhoneProperty = MutableProperty<String?>(nil)
    let userPasswordProperty = MutableProperty<String?>(nil)
    
    // MARK Private properties
    let loginInputValid = MutableProperty<Bool>(false)
    
    override func configureSignals() {
        errorSignalProducer = SignalProducer<(), NoError> { _,_ in }
        
        // MARK Login Producers
        let userPhoneSignalProducer = userPhoneProperty
            .producer
            .map { (text: String?) -> String in
                if let returnValue = text {
                    return returnValue;
                }
                return ""
        }
        
        let userPassworSignalProducer = userPasswordProperty
            .producer
            .map { (text: String?) -> String in
                if let returnValue = text {
                    return returnValue
                }
                return ""
        }
        
        loginInputValid <~ SignalProducer.combineLatest([userPhoneSignalProducer, userPassworSignalProducer])
            .flatMap(.latest) { credentionals in
                return SignalProducer<Bool, NoError> { observer, disposable in
                    
                    if let phone = credentionals.first, phone.characters.count == 12,
                        let password = credentionals.last, password.characters.count >= 1 {
                        observer.send(value: true)
                    } else {
                        observer.send(value: false)
                    }
                    observer.sendCompleted()
                }
        }
        
        // MARK Login Action
        loginSignalProducer = Action<(), UserEntity, ErrorEntity>(enabledIf: loginInputValid) { [unowned self] _ in
            return self.loginUser()
        }
        
        cocoaActionLogin = CocoaAction(loginSignalProducer, input:());
    }
    
    private func loginUser() -> SignalProducer<UserEntity, ErrorEntity> {
        
        return SignalProducer<UserEntity, ErrorEntity> { [weak self]
            observer, disposable in
            guard let strongSelf = self else { return }
            
            let phone = strongSelf.userPhoneProperty.value!
            let password = strongSelf.userPasswordProperty.value!
            
            networkProvider.request(.login(phone, password)) { [weak self] result in
                
                switch(result) {
                case let .success(response):
                    do {
                        if let json: [String:AnyObject] = try response.mapJSON() as? [String: AnyObject] {
                            debugPrint(json)
                            
                            if let status = json[MapperKey.success] as? Bool, status == true,
                                let userId = json[MapperKey.uid] as? String {
                                
                                
                                guard let item = Mapper<UserEntity>().map(JSON: json) else {
                                    debugPrint("can't parse data")
                                    return
                                }
                                
                                
                                //MARK:- remove DB data if it isn't current user
                                if let currentId = self?.currentId, String(currentId) != userId {
                                    do {
                                        let realm = try Realm()
                                        try realm.write {
                                            realm.deleteAll()
                                            
                                        }
                                    } catch {
                                        debugPrint("can't delete data from DB")
                                    }
                                }
                                
                                KeychainWrapper.standard.set(userId, forKey: Constants.currentUserId)
                                self?.setDataInDB(data: item)
                                
                                observer.send(value: item)
                                observer.sendCompleted()
                                
                                self?.openContainerVC()
                            } else {
                                guard let jsonArray = json[MapperKey.errors] as? [AnyObject],
                                    let errors = Mapper<ErrorEntity>().mapArray(JSONObject: jsonArray),
                                    let error = errors.first else {
                                        debugPrint("can't parse error data")
                                        observer.sendInterrupted()
                                        return
                                }
                                
                                debugPrint("error status")
                                observer.send(error: error)
                            }
                        }
                    } catch {
                        observer.sendInterrupted()
                    }
                case .failure(_):
                    debugPrint("error response")
                    observer.sendInterrupted()
                }
            }
        }
    }
    
    func registrationWithFacebook(userId: String, viewController: UIViewController) {
        GraphRequest(graphPath: Constants.fbGraphPath, parameters: Constants.fbParameters).start { [weak self] (_, result) in
            switch result {
            case .success(let response):
                if let json = response.dictionaryValue, let item = Mapper<FaceBookEntity>().map(JSON: json) {
                    debugPrint(item)
                    
                    self?.errorSignalProducer = SignalProducer<(), NoError> { [weak self] observer, disposable in
                        self?.registrationWithSocial(photo: item.photo, username: item.name, type: SocialType.facebook.rawValue, id: item.id)
                            .startWithSignal { [weak self] signal, _ in
                                
                                signal.observeCompleted { [weak self] in
                                    self?.loginWithFacebook(viewController: viewController).start()
                                }
                                
                                signal.observeInterrupted {
                                    observer.sendInterrupted()
                                }
                        }
                    }
                    
                    self?.errorSignalProducer.start()
                }
                
            case .failed(let error):
                debugPrint("registrationWithFacebook Request Failed: \(error)")
            }
        }
        
    }
    
    func loginWithFacebook(viewController: UIViewController) -> SignalProducer<(), NoError> {
        
        return SignalProducer<(), NoError> { [weak self]
            observer, disposable in
            
            let loginManager = LoginManager()
            loginManager.logOut()
            loginManager.logIn([ .publishActions ], viewController: viewController) { [weak self] loginResult in
                
                switch loginResult {
                case .failed(let error):
                    observer.send(error: error as! NoError)
                case .cancelled:
                    debugPrint("User cancelled login.")
                    observer.sendCompleted()
                    
                case .success( _, _, let token):
                    if let userId = token.userId {
                        self?.loginWithSocial(type: SocialType.facebook.rawValue, id: userId)
                            .startWithSignal { [weak self] signal, _ in
                                signal.observeCompleted {
                                    observer.sendCompleted()
                                }
                                
                                // registration user if he isn't registered
                                signal.observeFailed { [weak self] error in
                                    self?.registrationWithFacebook(userId: userId, viewController: viewController)
                                }
                                
                                signal.observeInterrupted {
                                    observer.sendInterrupted()
                                }
                        }
                    } else {
                        debugPrint("can't get facebook user id")
                        observer.sendInterrupted()
                    }
                }
            }
            
        }
    }
    
    func loginWithVkontakte(userId: String?) -> SignalProducer<UserEntity, ErrorEntity>  {
        
        return SignalProducer<UserEntity, ErrorEntity> { [weak self] observer, disposable in
            guard let userId = userId, let strongSelf = self else {
                return
            }
            
            VK.API.Users.get([VK.Arg.userId: userId, VK.Arg.fields: Constants.vkAvatarSize]).send(
                onSuccess: { response in
                    
                    if let json = response.arrayObject?.first as? [String: AnyObject] , let item = Mapper<VKEntity>().map(JSON: json) {
                        strongSelf.registrationWithSocial(photo: item.photo_50,
                                                          username: item.getFullName(),
                                                          type: SocialType.vk.rawValue,
                                                          id: item.id).startWithSignal { signal, _ in
                                                            signal.observeCompleted {
                                                                observer.sendCompleted()
                                                            }
                        }
                    } else {
                        debugPrint("can't parse data")
                    }
            },
                onError: {error in
                    debugPrint("incorrect data")}
            )
        }
    }
    
    func registarionWIthGoogle() {
        
    }
    
    func loginWithSocial(type: Int, id: String) -> SignalProducer<UserEntity, ErrorEntity> {
        
        return SignalProducer<UserEntity, ErrorEntity> { [weak self]
            observer, disposable in
            
            networkProvider.request(.loginWithSocial(type, id)) { [weak self] result in
                
                switch(result) {
                case let .success(response):
                    do {
                        if let json: [String:AnyObject] = try response.mapJSON() as? [String: AnyObject] {
                            
                            if let status = json[MapperKey.success] as? Bool, status == true,
                                let userId = json[MapperKey.uid] as? String {
                                
                                guard let item = Mapper<UserEntity>().map(JSON: json) else {
                                    debugPrint("can't parse data")
                                    return
                                }
                                
                                //MARK:- remove DB data if it isn't current user
                                if let currentId = self?.currentId, String(currentId) != userId {
                                    KeychainWrapper.standard.removeObject(forKey: Constants.dialog_id)
                                    KeychainWrapper.standard.removeObject(forKey: Constants.timestamp)
                                    KeychainWrapper.standard.removeObject(forKey: Constants.expertCallId)
                                    
                                    do {
                                        let realm = try Realm()
                                        try realm.write {
                                            realm.deleteAll()
                                        }
                                    } catch {
                                        debugPrint("can't delete data from DB")
                                    }
                                }
                                
                                KeychainWrapper.standard.set(userId, forKey: Constants.currentUserId)
                                self?.setDataInDB(data: item)
                                
                                observer.send(value: item)
                                observer.sendCompleted()
                                
                                self?.openContainerVC()
                            } else {
                                
                                guard let jsonArray = json[MapperKey.errors] as? [AnyObject],
                                    let errors = Mapper<ErrorEntity>().mapArray(JSONObject: jsonArray),
                                    let error = errors.first else {
                                        debugPrint("can't parse error data")
                                        observer.sendInterrupted()
                                        return
                                }
                                
                                if error.code == Constants.userNotFoundError {
                                    observer.send(error: error)
                                } else {
                                    observer.sendInterrupted()
                                }
                            }
                        }
                    } catch {
                        observer.sendInterrupted()
                    }
                case .failure(_):
                    observer.sendInterrupted()
                }
            }
        }
    }
    
    func registrationWithSocial(photo: String, username: String, type: Int, id: String) -> SignalProducer<UserEntity, NoError> {
        
        return SignalProducer<UserEntity, NoError> { [weak self]
            observer, disposable in
            
            networkProvider.request(.registerWithSocial(photo, username, type, id)) { [weak self] result in
                
                switch(result) {
                case let .success(response):
                    do {
                        if let json: [String:AnyObject] = try response.mapJSON() as? [String: AnyObject] {
                            
                            if let status = json[MapperKey.success] as? Bool, status == true,
                                let userId = json[MapperKey.uid] as? String {
                                KeychainWrapper.standard.set(userId, forKey: Constants.currentUserId)
                                
                                guard let item = Mapper<UserEntity>().map(JSON: json) else {
                                    debugPrint("can't parse data")
                                    return;
                                }
                                
                                observer.send(value: item)
                                observer.sendCompleted()
                                
                                self?.openContainerVC()
                            } else {
                                debugPrint("user not found")
                                observer.sendInterrupted()
                            }
                        }
                    } catch {
                        observer.sendInterrupted()
                    }
                case .failure(_):
                    observer.sendInterrupted()
                }
            }
        }
    }
    
}

//
//  ChangeDataViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/30/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit
import ReactiveSwift
import ReactiveCocoa
import MBProgressHUD
import Kingfisher

class ChangeDataViewController: UIViewController {
    
    @IBOutlet weak var topBackgroundView: UIView!
    
    @IBOutlet weak var avatarImageView: UIImageView!
    
    @IBOutlet weak var avatarButton: UIButton!
    @IBOutlet weak var saveButton: LoginButton!
    
    @IBOutlet weak var surnameTextField: LoginTextField!
    @IBOutlet weak var nameTextField: LoginTextField!
    @IBOutlet weak var phoneTextField: LoginTextField!
    @IBOutlet weak var passwordTextField: LoginTextField!
    @IBOutlet weak var confirmPasswordTextField: LoginTextField!
    
    @IBAction func changeAvatarButtonAction(_ sender: Any) {
        openAvatarMenuDialog()
    }
    
    let changeDataViewModel = ChangeUserDataViewModel()
    
    //MARK:- private properties
    private var user: UserEntity?
    private let imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureViews()
        configureNavigationBarButton()
        confgiureSignals()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.isTranslucent = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        navigationController?.navigationBar.isTranslucent = true
    }
    
    private func configureViews() {
        user = changeDataViewModel.getSelfEntity()
        title = Constants.editingTitle
        
        topBackgroundView.backgroundColor = ColorName.TopBarColor.color
        
        surnameTextField.bottomInfoLayer.backgroundColor            = UIColor.lightGray.cgColor
        nameTextField.bottomInfoLayer.backgroundColor               = UIColor.lightGray.cgColor
        phoneTextField.bottomInfoLayer.backgroundColor              = UIColor.lightGray.cgColor
        passwordTextField.bottomInfoLayer.backgroundColor           = UIColor.lightGray.cgColor
        confirmPasswordTextField.bottomInfoLayer.backgroundColor    = UIColor.lightGray.cgColor
        
        avatarButton.roundedView(border: false, borderColor: nil, borderWidth: nil, cornerRadius: nil)
        avatarImageView.roundedView(border: false, borderColor: nil, borderWidth: nil, cornerRadius: nil)
        
        let backgroundColor = ColorName.TopBarColor.color
        navigationController?.navigationBar.setBackgroundImage(backgroundColor.toImage(), for: .default)
        navigationController?.navigationBar.shadowImage = backgroundColor.toImage()
        
        imagePicker.delegate = self
        hideKeyboardWhenTappedAround()
        
        if let user = user {
            nameTextField.text    = user.name
            surnameTextField.text = user.username
            phoneTextField.text   = user.telephone
            
            changeDataViewModel.nameProperty.value    = user.name
            changeDataViewModel.surnameProperty.value = user.username
            changeDataViewModel.phoneProperty.value   = user.telephone
            
            if let userAvatarUrl = URL(string: user.user_photo) {
                avatarImageView.kf.setImage(with: ImageResource(downloadURL: userAvatarUrl, cacheKey: nil),
                                            placeholder: R.image.black_photo_icon(),
                                            options: nil,
                                            progressBlock: nil,
                                            completionHandler: nil)
            }
        }
    }
    
    private func configureNavigationBarButton() {
        let barButton = UIBarButtonItem(image: R.image.white_save_icon(),
                                        style: .plain,
                                        target: self,
                                        action: #selector(saveImageTapped))
        navigationItem.rightBarButtonItem = barButton
        barButton.reactive.isEnabled <~ changeDataViewModel.inputValidData
    }
    
    private func confgiureSignals() {
        saveButton.addTarget(changeDataViewModel.cocoaActionChangeData, action: CocoaAction<Any>.selector, for: .touchUpInside)
        
        // MARK:- button enable when data are valid
        saveButton.reactive.isEnabled <~ changeDataViewModel.inputValidData
        
        let nameProducer            = changeDataViewModel.racTextProducer(textField: nameTextField)
        let surnameProducer         = changeDataViewModel.racTextProducer(textField: surnameTextField)
        let phoneProducer           = changeDataViewModel.racTextProducer(textField: phoneTextField)
        let passwordProducer        = changeDataViewModel.racTextProducer(textField: passwordTextField)
        let confirmPasswordProduser = changeDataViewModel.racTextProducer(textField: confirmPasswordTextField)
        
        changeDataViewModel.nameProperty              <~ nameProducer
        changeDataViewModel.surnameProperty           <~ surnameProducer
        changeDataViewModel.phoneProperty             <~ phoneProducer
        changeDataViewModel.passwordProperty          <~ passwordProducer
        changeDataViewModel.confirmPasswordProperty   <~ confirmPasswordProduser
        
        saveButton.reactive
            .controlEvents(.touchUpInside)
            .observe { [weak self] _ in
                guard let strongSelf = self else { return }
                
                strongSelf.view.endEditing(true)
                MBProgressHUD.showAdded(to: strongSelf.view, animated: true)
        }
        
        changeDataViewModel.changeDataSignalProducer
            .events
            .observe(on: UIScheduler())
            .observeValues{ [weak self] (event) in
                guard let strongSelf = self else { return }
                MBProgressHUD.hide(for: strongSelf.view, animated: true)
                
                switch event {
                case .completed:
                    strongSelf.showSuccessMessage(title: nil, message: Constants.savedMessage)
                case .failed(let error):
                    strongSelf.showSmart24Error(error: error)
                default:
                    break
                }
        }
    }
    
    private func openImageSource() {
        imagePicker.allowsEditing = false;
        present(imagePicker, animated: true, completion: nil)
    }
    
    private func showSuccessMessage(title: String?, message: String) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { [weak self] action in
            self?.changeDataViewModel.backToPreviousVC()
        }
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
    }
    
    private  func openAvatarMenuDialog() {
        
        let alertController = UIAlertController(title: Constants.changePhoto, message: nil, preferredStyle: .actionSheet)
        alertController.view.tintColor = UIColor(named: .LoginButtonNormal)
        
        let cameraAction = UIAlertAction(title: Constants.camera, style: .default) { [weak self] (action: UIAlertAction!) in
            self?.imagePicker.sourceType = .camera
            self?.openImageSource();
        }
        
        let galleryAction = UIAlertAction(title: Constants.galery, style: .default) { [weak self]  (action: UIAlertAction!) in
            self?.imagePicker.sourceType = .photoLibrary
            self?.openImageSource()
        }
        
        let cancelAction = UIAlertAction(title: Constants.close, style: .destructive, handler: nil)
        
        alertController.addAction(cameraAction)
        alertController.addAction(galleryAction)
        alertController.addAction(cancelAction)
        
        changeDataViewModel.showAlertController(alertViewController: alertController)
    }
    
    func saveImageTapped() {
        view.endEditing(true)
        MBProgressHUD.showAdded(to: view, animated: true)
        
        changeDataViewModel.changeUserData()
            .observe(on: UIScheduler())
            .start { [weak self] event in
                guard let strongSelf = self else { return }
                MBProgressHUD.hide(for: strongSelf.view, animated: true)
                
                switch event {
                case .completed:
                    strongSelf.showSuccessMessage(title: nil, message: Constants.savedMessage)
                case .failed(let error):
                    strongSelf.showSmart24Error(error: error)
                default:
                    break
                }
        }
    }
}

extension ChangeDataViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    //MARK - UIImagePickerControllerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage else {
            dismiss(animated: true, completion: nil)
            return
        }
        
        avatarImageView.image = pickedImage.resizeImage(newWidth: 200)
        changeDataViewModel.userAvatarImage = pickedImage.resizeImage(newWidth: 200)
        dismiss(animated: true, completion: nil)
    }
}

//
//  MainViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/18/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit
import Reachability

class MainViewController: UIViewController {
    
    @IBOutlet weak var chatButton: UIButton!
    @IBOutlet weak var openMenuButton: UIButton!
    
    private let viewModel = ChatViewModel()
    private let reach = Reachability.forInternetConnection()
    
    @IBAction func openChatButtonAction(_ sender: Any) {
        viewModel.openChatVC()
        closeMenu(isEnabled: false)
    }
    @IBAction func makeCallButtonAction(_ sender: Any) {
        if let isReach = reach?.isReachable(), isReach {
            viewModel.openMakeCallVC(callId: nil)
            closeMenu(isEnabled: false)
        } else {
            showAlertMessage(title: nil, message: Constants.unreachableCreateBackup)
        }
    }
    @IBAction func makePhoneCallButtonAction(_ sender: Any) {
        if let url = URL(string: Constants.smart24UA_phone) {
            UIApplication.shared.openURL(url)
        }
        closeMenu(isEnabled: true)
    }
    @IBAction func openVideoButtonAction(_ sender: Any) {
        if let isReach = reach?.isReachable(), isReach {
            viewModel.openVideoVC(call: nil)
            closeMenu(isEnabled: false)
        } else {
            showAlertMessage(title: nil, message: Constants.unreachableCreateBackup)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        openMenuButton.setImage(R.image.menu_icon(), for: .normal)
        
        navigationController?.setNavigationBarHidden(true, animated: false)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationController?.navigationBar.barTintColor = ColorName.TopBarColor.color
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
        revealViewController().panGestureRecognizer().isEnabled = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    private func configureViews() {
        openMenuButton.addTarget(revealViewController(),
                                 action: #selector(revealViewController().revealToggle(_:)),
                                 for: .touchUpInside)
    }
    
    private func closeMenu(isEnabled: Bool) {
        if revealViewController().frontViewPosition == .right {
            revealViewController().revealToggle(animated: false)
        }
        
        revealViewController().panGestureRecognizer().isEnabled = isEnabled
    }
}

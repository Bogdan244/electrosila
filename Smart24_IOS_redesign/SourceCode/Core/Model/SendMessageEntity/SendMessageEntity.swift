//
//  SendMessageEntity.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 12/2/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import Foundation
import ObjectMapper

class SendMessageEntity: BaseEntity {
    
    dynamic var expert_name                 = ""
    dynamic var dialog_id                   = 0
    dynamic var time_of_receipt             = 0
    dynamic var expect                      = false
    dynamic var notice                      = ""
    dynamic var userId                      = ""
    dynamic var expert_id                   = ""
    dynamic var cc_id                       = ""
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        expert_name         <- map[MapperKey.expert_name]
        dialog_id           <- map[MapperKey.dialog_id]
        time_of_receipt     <- map[MapperKey.time_of_receipt]
        expect              <- map[MapperKey.expect]
        notice              <- map[MapperKey.notice]
        expert_id           <- map[MapperKey.expert_id]
        cc_id               <- map[MapperKey.cc_id]
    }
    
    override class func objectForMapping(map: Map) -> BaseMappable? {
        return SendMessageEntity()
    }
    
}

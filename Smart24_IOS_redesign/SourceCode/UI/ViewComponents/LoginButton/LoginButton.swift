//
//  LoginButton.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/18/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import Foundation
import UIKit

class LoginButton: UIButton {
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
        layer.cornerRadius = 5
        backgroundColor = ColorName.LoginButtonNormal.color
        
        isEnabled = true
    }
    
    override var isEnabled: Bool {
        didSet {
            if isEnabled {
                alpha = 1
            } else {
                alpha = 0.5
            }
        }
    }
    
    func setClearBackgrounStyle() {
        layer.borderWidth = 2
        layer.borderColor = ColorName.LoginButtonNormal.color.cgColor
        backgroundColor = UIColor.clear
        tintColor = ColorName.LoginButtonNormal.color
    }
}

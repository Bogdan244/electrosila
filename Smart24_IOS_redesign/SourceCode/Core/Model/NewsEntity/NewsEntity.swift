//
//  NewsEntity.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/24/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import Foundation
import ObjectMapper

class NewsEntity: BaseEntity {
    
    dynamic var title           = ""
    dynamic var newsdesc        = ""
    dynamic var header_image    = ""
    dynamic var publish_date    = ""
    dynamic var news_url        = ""
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        uid             <- map[MapperKey.news_id]
        title           <- map[MapperKey.title]
        newsdesc        <- map[MapperKey.description]
        header_image    <- map[MapperKey.header_image]
        publish_date    <- map[MapperKey.publish_date]
        news_url        <- map[MapperKey.news_url]
    }
    
    override class func objectForMapping(map: Map) -> BaseMappable? {
        return NewsEntity()
    }
    
}

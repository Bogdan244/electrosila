//
//  ProfileNotificationCell.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/28/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit

class ProfileNotificationCell: UITableViewCell, ReusableView, NibLoadableView {
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var redPointImageView: UIImageView!
    
    func updateViewWithModel(model: AnyObject) {
        guard let notification = model as? NotificationEntity else {
            return
        }
        
        descriptionLabel.text       = notification.title
        redPointImageView.isHidden  = notification.readStatus
        
        if let dateAsDouble = Double(notification.publish_date) {
            dateLabel.text = dateAsDouble.getStringDate(dateFormat: Constants.profileDateFormat)
        }
    }
    
}

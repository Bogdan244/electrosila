//
//  MenuHeaderView.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/23/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit
import Kingfisher

class MenuHeaderView: UIView, NibLoadableView {
    
    @IBOutlet weak var userAvatarImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var settingsButton: UIButton!
    @IBOutlet weak var avatarButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        backgroundColor = .clear
        userAvatarImageView.roundedView(border: false, borderColor: nil, borderWidth: nil, cornerRadius: nil)
        avatarButton.roundedView(border: false, borderColor: nil, borderWidth: nil, cornerRadius: nil)
    }
    
    func updateViewWithModel(model: AnyObject?) {
        guard let owner = model as? UserEntity else {
            return
        }
        
        usernameLabel.text = owner.name
        phoneNumberLabel.text = owner.telephone

        if let userAvatarUrl = URL(string: owner.user_photo) {
            userAvatarImageView.kf.setImage(with: ImageResource(downloadURL: userAvatarUrl, cacheKey: nil),
                                            placeholder: nil,
                                            options: nil,
                                            progressBlock: nil,
                                            completionHandler: nil)
        }
    }
}


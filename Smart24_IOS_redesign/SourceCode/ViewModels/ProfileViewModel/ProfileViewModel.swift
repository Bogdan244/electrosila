//
//  ProfileViewModel.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/28/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import Foundation
import ReactiveCocoa
import ReactiveSwift
import ObjectMapper
import RealmSwift
import enum Result.NoError

class ProfileViewModel: GeneralViewModel {
    
    var notifications: ReactiveSwift.Property<[NotificationEntity]?> { return Property(_notifications) }
    var activeServices: ReactiveSwift.Property<[ActiveServicesEntity]?> { return Property(_activeServices) }
    
    private var _notifications = MutableProperty<[NotificationEntity]?>(nil)
    private var _activeServices = MutableProperty<[ActiveServicesEntity]?>(nil)
    
    func getNotifications(userId: String) -> SignalProducer<NotificationEntity, NoError> {
        
        return SignalProducer<NotificationEntity, NoError> { [weak self]
            observer, disposable in
            
            self?._notifications.value = self?.getNotifications()
            
            networkProvider.request(.notifications(userId)) { [weak self] result in
                
                switch(result) {
                case let .success(response):
                    do {
                        if let json: [String:AnyObject] = try response.mapJSON() as? [String: AnyObject] {
                            
                            if let status = json[MapperKey.success] as? Bool, status == true,
                                let jsonArray = json[MapperKey.notifications] as? [AnyObject] {
                                
                                guard let items = Mapper<NotificationEntity>().mapArray(JSONObject: jsonArray) else {
                                    debugPrint("can't parse data")
                                    return
                                }
                                
                                //MARK:- add new messages in DB
                                items.forEach { [weak self] element in
                                    
                                    if self?._notifications.value == nil {
                                        self?.setDataInDB(data: element)
                                        self?._notifications.value = [element]
                                    } else if let isContains = self?._notifications.value?.contains(element), isContains == false {
                                        self?.setDataInDB(data: element)
                                        self?._notifications.value?.append(element)
                                    }
                                }
                                
                                observer.sendCompleted()
                                
                            } else {
                                debugPrint("error status")
                                observer.sendInterrupted()
                            }
                        }
                    } catch {
                        observer.sendInterrupted()
                    }
                case .failure(_):
                    observer.sendInterrupted()
                }
            }
        }
    }
    
    func activateService(userId: String, activationCode: String) -> SignalProducer<(), ErrorEntity> {
        
        return SignalProducer<(), ErrorEntity> { observer, disposable in
            
            networkProvider.request(.activateService(userId, activationCode)) { result in
                
                switch(result) {
                case let .success(response):
                    do {
                        if let json: [String:AnyObject] = try response.mapJSON() as? [String: AnyObject] {
                            
                            if let status = json[MapperKey.success] as? Bool, status == true {
                                
                                debugPrint("success activation")
                                observer.sendCompleted()
                                
                            } else {
                                guard let jsonArray = json[MapperKey.errors] as? [AnyObject],
                                    let errors = Mapper<ErrorEntity>().mapArray(JSONObject: jsonArray),
                                    let error = errors.first else {
                                        debugPrint("can't parse data")
                                        observer.sendInterrupted()
                                        return
                                }
                                
                                observer.send(error: error)
                            }
                        }
                    } catch {
                        observer.sendInterrupted()
                    }
                case .failure(_):
                    observer.sendInterrupted()
                }
            }
        }
    }
    
    
    
    func getActiveServices(userId: String) -> SignalProducer<(), ErrorEntity> {
        
        return SignalProducer<(), ErrorEntity> { [weak self] observer, disposable in
            
            let density = UIScreen.main.scale == 2.0 ? SmartDensity.for2x.rawValue : SmartDensity.for3x.rawValue
            
            networkProvider.request(.getActiveServices(userId, density)) { [weak self] result in
                
                switch(result) {
                case let .success(response):
                    do {
                        if let json: [String:AnyObject] = try response.mapJSON() as? [String: AnyObject] {
                            //debugPrint(json)
                            if let status = json[MapperKey.success] as? Bool, status == true {
                                
                                guard let jsonArray = json[MapperKey.services] as? [AnyObject],
                                    let item = Mapper<ActiveServicesEntity>().mapArray(JSONObject: jsonArray) else {
                                        debugPrint("can't parse data")
                                        observer.sendInterrupted()
                                        return
                                }

                                self?._activeServices.value = item
                                observer.sendCompleted()
                                
                            } else {
                                guard let jsonArray = json[MapperKey.errors] as? [AnyObject],
                                    let errors = Mapper<ErrorEntity>().mapArray(JSONObject: jsonArray),
                                    let error = errors.first else {
                                        debugPrint("can't parse data")
                                        observer.sendInterrupted()
                                        return
                                }
                                
                                observer.send(error: error)
                            }
                        }
                    } catch {
                        observer.sendInterrupted()
                    }
                case .failure(_):
                    observer.sendInterrupted()
                }
            }
        }
    }
    
    func syncNotifications(userId: Int, notificationId: Int, action: String) -> SignalProducer<(), ErrorEntity> {
        
        return SignalProducer<(), ErrorEntity> { observer, disposable in
            
            networkProvider.request(.syncNotifications(userId, notificationId, action)) { result in
                
                switch(result) {
                case let .success(response):
                    do {
                        if let json: [String:AnyObject] = try response.mapJSON() as? [String: AnyObject] {
                            
                            if let status = json[MapperKey.success] as? Bool, status == true {
                                
                                observer.sendCompleted()
                                
                            } else {
                                guard let jsonArray = json[MapperKey.errors] as? [AnyObject],
                                    let errors = Mapper<ErrorEntity>().mapArray(JSONObject: jsonArray),
                                    let error = errors.first else {
                                        debugPrint("can't parse data")
                                        observer.sendInterrupted()
                                        return
                                }
                                
                                observer.send(error: error)
                            }
                        }
                    } catch {
                        observer.sendInterrupted()
                    }
                case .failure(_):
                    observer.sendInterrupted()
                }
            }
        }
    }
    
    func deleteNotification(userId: Int, notificationId: Int, action: String) -> SignalProducer<(), ErrorEntity> {
        
        return SignalProducer<(), ErrorEntity> { [weak self] observer, disposable in
            
            networkProvider.request(.syncNotifications(userId, notificationId, action)) { [weak self] result in
                
                switch(result) {
                case let .success(response):
                    do {
                        if let json: [String:AnyObject] = try response.mapJSON() as? [String: AnyObject] {
                            
                            if let status = json[MapperKey.success] as? Bool, status == true {
                                
                                self?.deleteNotificationFromDB(notificationId: String(notificationId))
                                
                                observer.sendCompleted()
                                
                            } else {
                                guard let jsonArray = json[MapperKey.errors] as? [AnyObject],
                                    let errors = Mapper<ErrorEntity>().mapArray(JSONObject: jsonArray),
                                    let error = errors.first else {
                                        debugPrint("can't parse data")
                                        observer.sendInterrupted()
                                        return
                                }
                                
                                observer.send(error: error)
                            }
                        }
                    } catch {
                        observer.sendInterrupted()
                    }
                case .failure(_):
                    observer.sendInterrupted()
                }
            }
        }
    }
    
    func readNotification(message: NotificationEntity) {
        do {
            let realm = try Realm()
            try realm.write {
                message.readStatus = true
                realm.add(message, update: true)
            }
        } catch {}
    }
    
    func unreadMessagesCount() -> Int? {
        return _notifications.value?.filter({ !$0.readStatus }).count
    }
    
    private func deleteNotificationFromDB(notificationId: String) {
        do {
            let realm = try Realm()
            if let object = realm.object(ofType: NotificationEntity.self, forPrimaryKey: notificationId) {
                try realm.write {
                    realm.delete(object)
                }
            }
        } catch { }
        
        _notifications.value = getNotifications()
    }
    
    private func getNotifications() -> [NotificationEntity]? {
        do {
            let realm = try Realm()
            let notificationsEntity = realm.objects(NotificationEntity.self).toArray(ofType: NotificationEntity.self)
            return notificationsEntity.count > 0 ? notificationsEntity : nil
        } catch {
            return nil
        }
    }
}

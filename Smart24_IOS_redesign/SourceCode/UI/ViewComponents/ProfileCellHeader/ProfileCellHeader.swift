//
//  ProfileCellHeader.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/28/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit

class ProfileCellHeader: UIView, NibLoadableView {

    @IBOutlet weak var activateButton: LoginButton!
    @IBOutlet weak var notificationsLabel: UILabel!
}

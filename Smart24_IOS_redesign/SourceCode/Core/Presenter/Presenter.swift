//
//  Presenter.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/15/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit

protocol Presenter {
    
    func presentLoginVC()
    func backToPreviousVC()
    func openRootVC()
    
    func openLoginVC()
    func openRegistrationVC()
    func openForgotPasswordVC()
    func openContainerVC()
    func openMainVC()
    func openNewsVC()
    func openUtilityVC()
    func openProfileVC()
    func openNotificationVC(message: NotificationEntity)
    func openChangeDataVC()
    func openBackupVC()
    func openAlertVC(alertViewController: UIAlertController)
    func openChatVC()
    func presentChatVC()
    func openHowItWorkTBC()
    func openDevelopersVC()
    func openBackupProgressVC(status: BackupState)
    func openBackupPerionVC()
    func openMakeCallVC(callId: String?)
    func openNewsDescriptionVC(news: NewsEntity)
    func openVideoVC(call: VSLCall?)
}

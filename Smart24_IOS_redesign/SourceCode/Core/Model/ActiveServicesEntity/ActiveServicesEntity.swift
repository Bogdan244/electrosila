//
//  ActiveServicesEntity.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/29/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import Foundation
import ObjectMapper

class ActiveServicesEntity: BaseEntity {
    
    dynamic var icon                    = ""
    dynamic var avto_prolong            = false
    dynamic var name                    = ""
    dynamic var serial_keys             = ""
    dynamic var expired_time: Double    = 0
    dynamic var program_categories      = ""
    dynamic var program_list            = ""
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        uid                 <- map[MapperKey.service_id]
        icon                <- map[MapperKey.icon]
        avto_prolong        <- map[MapperKey.avto_prolong]
        name                <- map[MapperKey.name]
        serial_keys         <- map[MapperKey.serial_keys]
        expired_time        <- map[MapperKey.expired_time]
        program_categories  <- map[MapperKey.prog_categories]
        program_list        <- map[MapperKey.program_list]
    }
    
    override class func objectForMapping(map: Map) -> BaseMappable? {
        return ActiveServicesEntity()
    }
    
}

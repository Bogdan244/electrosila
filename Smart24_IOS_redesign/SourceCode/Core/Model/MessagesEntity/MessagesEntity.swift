//
//  MessagesEntity.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 12/2/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import Foundation
import ObjectMapper

class MessagesEntity: BaseEntity {
    
    dynamic var message           = ""
    dynamic var expert_name       = ""
    dynamic var expert_avatar     = ""
    dynamic var time              = 0
    dynamic var type              = ""
    dynamic var expert_id         = ""
    dynamic var cc_id             = ""
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        message         <- map[MapperKey.message]
        expert_name     <- map[MapperKey.expert_name]
        expert_avatar   <- map[MapperKey.expert_avatar]
        time            <- map[MapperKey.time]
        type            <- map[MapperKey.type]
        expert_id       <- map[MapperKey.expert_id]
        cc_id           <- map[MapperKey.cc_id]
    }
    
    override class func primaryKey() -> String? {
        return "time"
    }
    
    override class func objectForMapping(map: Map) -> BaseMappable? {
        return MessagesEntity()
    }
    
}

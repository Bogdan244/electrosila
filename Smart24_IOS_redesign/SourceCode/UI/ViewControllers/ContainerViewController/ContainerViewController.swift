//
//  ContainerViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/15/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit
import SWRevealViewController
import ReactiveCocoa
import ReactiveSwift
import Reachability

class ContainerViewController: SWRevealViewController {
    
    var chatTimer: Timer?
    var chatViewModel = ChatViewModel()
    
    var reach: Reachability?
    
    private let timerInterval: Double = 10
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        configureChatTimer()
        
        if let controller = R.storyboard.main.mainViewController() {
            let navigationController = Smart24NavigationController(rootViewController: controller)
            frontViewController = navigationController
        }
        
        // Add left menu
        if let controller = R.storyboard.main.menuTableViewController() {
            rearViewController = controller
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        reach = Reachability.forInternetConnection()
        reach?.reachableOnWWAN = true
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(reachabilityChanged),
                                               name: NSNotification.Name.reachabilityChanged,
                                               object: nil)
        reach?.startNotifier()
    }
    
    func reachabilityChanged(notification: NSNotification) {
        if self.reach!.isReachableViaWiFi() || self.reach!.isReachableViaWWAN() {
            debugPrint("reachable connect")
        } else {
            showAlertMessage(title: nil, message: Constants.unreachableMessage)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated);
        
        if chatTimer != nil {
            chatTimer?.invalidate()
            chatTimer = nil
        }
        
        reach?.stopNotifier()
    }
    
    func configureChatTimer() {
        chatTimer = Timer.scheduledTimer(timeInterval: timerInterval, target: self, selector: #selector(updateChat), userInfo: nil, repeats: true)
    }
    
    func updateChat() {
        
        //MARK:- if ChatVC is current VC then update chat view
        if let chatVC = frontViewController.childViewControllers.last as? ChatViewController {
            chatVC.loadMessages(getDataFromDB: false)
            return
        }
        
        //MARK:- if VideoVC is current VC then update chat view
        if let videoVC = frontViewController.childViewControllers.last as? VideoViewController,
            let chatVC = videoVC.childViewControllers.first as? ChatViewController {
            chatVC.loadMessages(isVideo: IsVideoStart.start.rawValue, getDataFromDB: false)
            return
        }
        
        var dialogId =  Constants.emptyDialogId
        if let id = chatViewModel.dialogId {
            dialogId = id
        }
        
        //MARK:- if get new message then show local notification
        chatViewModel.getAnswer(dialog_id: dialogId, timestamp: chatViewModel.dialogTimestamp, isVideo: IsVideoStart.not.rawValue, getDataFromDB: false)
            .producer
            .start { [weak self] event in
                switch event {
                case .value(let value):
                    self?.createNotification(message: value)
                case .failed(let error):
                    DispatchQueue.main.asyncAfter(deadline: .now()) { [weak self] in
                        self?.showErrorMessage(error: error)
                    }
                default:
                    debugPrint("message observing interrupted")
                }
        }
    }
    
    private func createNotification(message: JSQMessageEntity) {
        let localNotification = UILocalNotification()
        localNotification.alertBody = message.text
        localNotification.alertTitle = message.senderDisplayName
        localNotification.timeZone = TimeZone.current
        localNotification.fireDate = Date(timeIntervalSinceNow: 1)
        localNotification.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber + 1
        UIApplication.shared.scheduleLocalNotification(localNotification)
    }
    
    private func showErrorMessage(error: ErrorEntity) {
        if error.code == Constants.notAuthError {
            chatTimer?.invalidate()
            let alertController = UIAlertController(title: nil, message: error.data, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default) { [weak self] action in
                self?.chatViewModel.presentLoginVC()
            }
            alertController.addAction(okAction)
            present(alertController, animated: true, completion: nil)
        }
    }
}

//
//  UtilityViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/25/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit

class UtilityViewController: UIViewController {
    
    @IBOutlet weak var downloadButton: LoginButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        downloadButton.setClearBackgrounStyle()
        downloadButton.addTarget(self, action: #selector(openWebView), for: .touchUpInside)
    }
    
    func openWebView() {
       if let url = URL(string: Constants.teamViewLink) {
            UIApplication.shared.openURL(url)
        }
    }
}

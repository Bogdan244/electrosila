//
//  ChangeUserDataViewModel.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/30/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import Foundation
import ReactiveCocoa
import ReactiveSwift
import ObjectMapper
import enum Result.NoError

class ChangeUserDataViewModel: GeneralViewModel {
    
    var userAvatarImage: UIImage?
    var cocoaActionChangeData: CocoaAction<Any>!
    var changeDataSignalProducer: Action<(), (), ErrorEntity>!
    var inputValidData = MutableProperty<Bool>(false);
    
    let nameProperty            = MutableProperty<String?>("")
    let surnameProperty         = MutableProperty<String?>("")
    let phoneProperty           = MutableProperty<String?>("")
    let passwordProperty        = MutableProperty<String?>("")
    let confirmPasswordProperty = MutableProperty<String?>("")
    
    var nameSignalProducer: SignalProducer<Bool, NoError>!
    var surnameSignalProducer: SignalProducer<Bool, NoError>!
    var phoneSignalProducer: SignalProducer<Bool, NoError>!
    var passwordSignalProducer: SignalProducer<Bool, NoError>!
    var confirmPaswordSignalProducer: SignalProducer<Bool, NoError>!
    
    override func configureSignals() {
        
        // MARK Register Producers
        nameSignalProducer = nameProperty
            .producer
            .map { (text: String?) -> Bool in
                guard let value = text, value.characters.count >= 1 && value.characters.count <= 20 || value == "" else {
                    return false
                }
                return true
        }
        
        surnameSignalProducer = surnameProperty
            .producer
            .map { (text: String?) -> Bool in
                guard let value = text, value.characters.count >= 1 && value.characters.count <= 20 || value == "" else {
                    return false
                }
                return true
        }
        
        phoneSignalProducer = phoneProperty
            .producer
            .map { (text: String?) -> Bool in
                guard let value = text, value.characters.count >= 1 || value == "" else {
                    return false
                }
                return true
        }
        
        passwordSignalProducer = passwordProperty
            .producer
            .map { (text: String?) -> Bool in
                guard let value = text, value.characters.count >= 1 && value.characters.count <= 32 || value == "" else {
                    return false
                }
                return true
        }
        
        confirmPaswordSignalProducer = confirmPasswordProperty
            .producer
            .map { (text: String?) -> Bool in
                guard let value = text, value == self.passwordProperty.value || value == "" else {
                    return false
                }
                return true
        }
        
        inputValidData <~ SignalProducer.combineLatest([nameSignalProducer, surnameSignalProducer, phoneSignalProducer, passwordSignalProducer, confirmPaswordSignalProducer])
            .flatMap((.latest)) { credentionals in
                return SignalProducer<Bool, NoError> {
                    
                    observer, disposable in
                    
                    if self.isTextFieldEmpty() && credentionals[0] && credentionals[1] && credentionals[2] && credentionals[3] && credentionals[4]{
                        observer.send(value: true)
                    } else {
                        observer.send(value: false)
                    }
                    observer.sendCompleted()
                }
        }
        
        // MARK Registration Action
        changeDataSignalProducer = Action<(),(), ErrorEntity> { [unowned self] _ in
            return self.changeUserData()
        }
        
        cocoaActionChangeData = CocoaAction(changeDataSignalProducer, input:())
    }
    
    func changeUserData() -> SignalProducer<(), ErrorEntity> {
        
        return SignalProducer<(), ErrorEntity> { [weak self]
            observer, disposable in
            guard let strongSelf = self else { return }
            
            let name            = strongSelf.nameProperty.value!
            let surname         = strongSelf.surnameProperty.value!
            let phone           = strongSelf.phoneProperty.value!
            let password        = strongSelf.passwordProperty.value!
            let confirmPassword = strongSelf.confirmPasswordProperty.value!
            let stringUserId    = String(strongSelf.currentId)
            var base64ImageData: String?
            
            
            if let userImage = strongSelf.userAvatarImage, let imageData = UIImagePNGRepresentation(userImage) {
                base64ImageData = imageData.base64EncodedString(options: .lineLength64Characters)
            }
            
            networkProvider.request(.changeUserData(stringUserId, base64ImageData, name, surname, phone, password, confirmPassword)) { result in
                switch(result) {
                case let .success(response):
                    do {
                        if let json: [String:AnyObject] = try response.mapJSON() as? [String:AnyObject] {
                            
                            if let result = json[MapperKey.success] as? Bool, result == true {
                                self?.updateUserInfo()
                                    .start { events in
                                        switch events {
                                        case .completed:
                                            observer.sendCompleted()
                                        case .interrupted:
                                            observer.sendInterrupted()
                                        case .failed(let error):
                                            observer.send(error: error)
                                        case .value(_):
                                            break
                                        }
                                    }
                                
                               
                            } else {
                                guard let jsonArray = json[MapperKey.errors] as? [AnyObject],
                                    let errors = Mapper<ErrorEntity>().mapArray(JSONObject: jsonArray),
                                    let error = errors.first else {
                                        debugPrint("can't parse data")
                                        observer.sendInterrupted()
                                        return
                                }
                                
                                observer.send(error: error)
                            }
                        }
                    } catch {
                        observer.sendInterrupted()
                    }
                    
                case .failure(_):
                    observer.sendInterrupted()
                }
            }
        }
        
    }
    
    private func updateUserInfo() -> SignalProducer<(), ErrorEntity> {
        
        return SignalProducer<(), ErrorEntity> { [weak self]
            observer, disposable in
            
            guard let strongSelf = self else { return }
            let stringUserId    = String(strongSelf.currentId)
            
            networkProvider.request(.getUserInfo(stringUserId)) { result in
                switch(result) {
                case let .success(response):
                    do {
                        if let json: [String:AnyObject] = try response.mapJSON() as? [String:AnyObject] {
                            debugPrint(json)
                            if let result = json[MapperKey.success] as? Bool, result == true {
                                
                                guard let item = Mapper<UserEntity>().map(JSON: json) else {
                                    debugPrint("can't parse data")
                                    return
                                }

                                item.uid = stringUserId
                                debugPrint("item \(item)")
                                self?.setDataInDB(data: item)
                                
                                observer.sendCompleted()
                            } else {
                                guard let jsonArray = json[MapperKey.errors] as? [AnyObject],
                                    let errors = Mapper<ErrorEntity>().mapArray(JSONObject: jsonArray),
                                    let error = errors.first else {
                                        debugPrint("can't parse data")
                                        observer.sendInterrupted()
                                        return
                                }
                                
                                observer.send(error: error)
                            }
                        }
                    } catch {
                        observer.sendInterrupted()
                    }
                    
                case .failure(_):
                    observer.sendInterrupted()
                }
            }
        }
    }
    
    private func isTextFieldEmpty() -> Bool {
        if  self.nameProperty.value! != "" &&
            self.surnameProperty.value! != "" &&
            self.phoneProperty.value! != "" {
            return true
        } else {
            return false
        }
    }
}

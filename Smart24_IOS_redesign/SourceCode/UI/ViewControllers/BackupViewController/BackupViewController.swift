 //
 //  BackupViewController.swift
 //  Smart24_IOS_redesign
 //
 //  Created by Vitya on 12/1/16.
 //  Copyright © 2016 Vitya. All rights reserved.
 //
 
 import UIKit
 import MBProgressHUD
 import ReactiveSwift
 import SwiftKeychainWrapper
 import Reachability
 
 enum CloudSyncType: Error {
    case success
    case error
    case unavailable
    case noContacts
    case restoreSuccess
    case restoreFailure
    case backgroundBackupAccessDenied
    case backgroundBackupAccessLimited
 }
 
 enum BackupState {
    case createBackup
    case getBackup
 }
 
 class BackupViewController: UIViewController {
    
    @IBOutlet weak var restoreLastDataButton: LoginButton!
    @IBOutlet weak var saveDataButton: LoginButton!
    
    @IBOutlet weak var lastBackupDate: UILabel!
    @IBOutlet weak var nextBackupDate: UILabel!
   // @IBOutlet weak var backupPeriodLabel: UILabel!
    
    //@IBOutlet weak var activateBackupPetiodSwitch: UISwitch!
    @IBOutlet weak var selectedAdressBookView: UIView!
    @IBOutlet weak var bookCheckImageView: UIImageView!
    
    // MARK: Actions
 /*   @IBAction func periodButtonAction(_ sender: Any) {
        viewModel.openBackupPerionVC()
    }
    @IBAction func periodSwitchAction(_ sender: UISwitch) {
        KeychainWrapper.standard.set(sender.isOn, forKey: Constants.setOnBackupPeriod)
    } */
    
    let viewModel = BackupViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureView()
        //configureBackupPeriod()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        configureSignals()
        
        if let backupButtonState = UserDefaults.standard.value(forKey: Constants.backupButtonState) as? Bool, backupButtonState == false {
            // set default if first start
            UserDefaults.standard.set(false, forKey: Constants.backupButtonState)
            UserDefaults.standard.set(0, forKey: Constants.backupButtonDayIndex)
            UserDefaults.standard.synchronize()
        }
    }
    
    private func configureView() {
        saveDataButton.addTarget(self, action: #selector(saveData), for: .touchUpInside)
        restoreLastDataButton.addTarget(self, action: #selector(toggleRestoreLastCopyButton), for: .touchUpInside)
        restoreLastDataButton.isEnabled = false
        
        let adressBookGesture = UITapGestureRecognizer(target: self, action: #selector(backupAdressBookTap))
        selectedAdressBookView.addGestureRecognizer(adressBookGesture)
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        nextBackupDate.isHidden = true
        setNextDateBuckupInfo()
    }
    
    func setNextDateBuckupInfo() {
        if let backupDateToString = UserDefaults.standard.value(forKey: Constants.backupDate) as? Double {
            nextBackupDate.text = secondsToDate(seconds: Int64(backupDateToString))
        }
    }
    
    func saveData() {
        let reach = Reachability.forInternetConnection()
        
        if let isReach = reach?.isReachable(), isReach {
            viewModel.openBackupProgressVC(status: .createBackup)
        } else {
            showAlertMessage(title: nil, message: Constants.unreachableCreateBackup)
        }
    }
    
    func toggleRestoreLastCopyButton() {
        let reach = Reachability.forInternetConnection()
        
        if let isReach = reach?.isReachable(), isReach {
            viewModel.openBackupProgressVC(status: .getBackup)
        } else {
            showAlertMessage(title: nil, message: Constants.unreachableRestoreBackup)
        }
    }
    
    private func secondsToDate(seconds: Int64) -> String {
        let date = Date(timeIntervalSince1970: Double(seconds))
        let formatter = DateFormatter()
        
        formatter.calendar = Calendar.current
        formatter.locale = NSLocale.current
        formatter.dateFormat = Constants.backupDateFormat
        
        return formatter.string(from: date)
    }
    
  /*  private func configureBackupPeriod() {
        if let isSwitchOn = KeychainWrapper.standard.bool(forKey: Constants.setOnBackupPeriod), isSwitchOn == true {
            activateBackupPetiodSwitch.isOn = true
        } else {
            activateBackupPetiodSwitch.isOn = false
        }
        
        guard let value = KeychainWrapper.standard.integer(forKey: Constants.backupPeriod),
            let period = BackupPeriod(rawValue: value) else {
                return
        }
        
        switch period {
        case .day:
            backupPeriodLabel.text = Constants.dayBackupMessage
        case .week:
            backupPeriodLabel.text = Constants.weekBackupMessage
        case .month:
            backupPeriodLabel.text = Constants.monthBackupMessage
        case .year:
            backupPeriodLabel.text = Constants.yearBackupMessage
        }
    } */
    
    private func configureSignals() {
        
        viewModel.getInfoBackup()
            .observe(on: UIScheduler())
            .start { [weak self] event in
                guard let strongSelf = self else {return}
                
                switch event {
                case .value(let date):
                    let newDate = strongSelf.secondsToDate(seconds: Int64(date))
                    strongSelf.lastBackupDate.text = "Последняя активация \(newDate)"
                    strongSelf.restoreLastDataButton.isEnabled = true
                case .interrupted:
                    strongSelf.lastBackupDate.text = "Последняя активация"
                default:
                    break
                }
                
                MBProgressHUD.hide(for: strongSelf.view, animated: true)
        }
    }
    
    func backupAdressBookTap() {
        if bookCheckImageView.isHidden {
            bookCheckImageView.isHidden = false
            restoreLastDataButton.isEnabled = true
            saveDataButton.isEnabled = true
        } else {
            bookCheckImageView.isHidden = true
            restoreLastDataButton.isEnabled = false
            saveDataButton.isEnabled = false
        }
    }
 }

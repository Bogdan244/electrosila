//
//  NewsDescriptionViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 12/24/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit
import Kingfisher
import MBProgressHUD

class NewsDescriptionViewController: UIViewController {
    
    @IBOutlet weak var newsImageView: UIImageView!
    @IBOutlet weak var newsWebView: UIWebView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    var news: NewsEntity?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureNavigationViewController()
        configureView()
    }
    
    private func configureNavigationViewController() {
        let sharing = UIBarButtonItem(image: R.image.sharing_icon(), style: .plain , target: self, action: #selector(sheringButtonTapped))
        navigationItem.rightBarButtonItems = [sharing]
    }
    
    private func configureView() {
        title = "Сделай сам"
        
        if let news = news {
            titleLabel.text = news.title
            dateLabel.text = news.publish_date.getDate(dateFormat: "dd MMM YYYY")
            
            if let newsUrl = URL(string: news.news_url) {
                newsWebView.loadRequest(URLRequest(url: newsUrl))
            }
            
            if let newsUrl = URL(string: news.header_image) {
                newsImageView.kf.setImage(with: ImageResource(downloadURL: newsUrl, cacheKey: nil),
                                          placeholder: nil,
                                          options: nil,
                                          progressBlock: nil,
                                          completionHandler: nil)
            }
        }
    }
    
    func sheringButtonTapped() {
        if let news = news {
            let vc = UIActivityViewController(activityItems: [news.title], applicationActivities: [])
            present(vc, animated: true)
        }
    }
}

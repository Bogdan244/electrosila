//
//  PresenterImpl.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/15/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit
//import SwiftKeychainWrapper

final class PresenterImpl: Presenter {
    
    private let storyboard = R.storyboard.main();
    private let router: Router
    
    
    init() {
        guard let topController = UIApplication.topNavigationViewController() else {
            fatalError("Couldn't init view stack")
        }
        router = RouterImpl(rootController: topController as! Smart24NavigationController);
    }
    
    func openLoginVC() {
        if let vc = R.storyboard.main.loginViewController() {
            router.push(controller: vc, animated: true)
        }
    }
    
    func openRegistrationVC() {
        if let vc = R.storyboard.main.registrationViewController() {
            router.push(controller: vc, animated: true)
        }
    }
    
    func openForgotPasswordVC() {
        if let vc = R.storyboard.main.forgotPasswordViewController() {
            router.push(controller: vc, animated: true)
        }
    }
    
    func backToPreviousVC() {
        router.popController(animated: true)
    }
    
    func openRootVC() {
        router.popToRootViewController(animated: true)
    }
    
    func openContainerVC() {
        if let vc = R.storyboard.main.containerViewController() {
            router.present(controller: vc, animated: true)
        }
    }
    
    func openMainVC() {
        if let vc = R.storyboard.main.mainViewController() {
            router.present(controller: vc, animated: true)
        }
    }
    
    func presentLoginVC() {
        if let vc = R.storyboard.main.loginViewController() {
            router.changeRootViewController(controller: vc)
        }
    }
    
    func openNewsVC() {
        if let vc = R.storyboard.main.newsViewController() {
            router.push(controller: vc, animated: true)
        }
    }
    
    func openUtilityVC() {
        if let vc = R.storyboard.main.utilityViewController() {
            router.push(controller: vc, animated: true)
        }
    }
    
    func openProfileVC() {
        if let vc = R.storyboard.main.profileViewController() {
            router.push(controller: vc, animated: true)
        }
    }
    
    func openNotificationVC(message: NotificationEntity) {
        if let vc = R.storyboard.main.notificationViewController() {
            vc.message = message
            router.push(controller: vc, animated: true)
        }
    }
    
    func openChangeDataVC() {
        if let vc = R.storyboard.main.changeDataViewController() {
            router.push(controller: vc, animated: true)
        }
    }
    
    func openBackupVC() {
        if let vc = R.storyboard.main.backupViewController() {
            router.push(controller: vc, animated: true)
        }
    }
    
    func openAlertVC(alertViewController: UIAlertController) {
        router.openAlertViewController(alertViewController: alertViewController)
    }
    
    func openChatVC() {
        if let vc = R.storyboard.main.chatViewController() {
            router.push(controller: vc, animated: true)
        }
    }
    
    func presentChatVC() {
        if let vc = R.storyboard.main.chatViewController() {
            router.pushVCToRoot(viewController: vc, animated: true)
        }
    }
    
    func openHowItWorkTBC() {
        if let vc = R.storyboard.main.howItWorkController() {
            router.push(controller: vc, animated: true)
        }
    }
    
    func openDevelopersVC() {
        if let vc = R.storyboard.main.developersViewController() {
            router.push(controller: vc, animated: true)
        }
    }
    
    func openBackupProgressVC(status: BackupState) {
        if let vc = R.storyboard.main.backupProgressViewController() {
            vc.backupStatus = status
            router.push(controller: vc, animated: true)
        }
    }
    
    func openBackupPerionVC() {
        if let vc = R.storyboard.main.backupPerionViewController() {
            router.push(controller: vc, animated: true)
        }
    }
    
    func openMakeCallVC(callId: String?) {
        if let vc = R.storyboard.main.makeCallViewController() {
            if let callId = callId {
                vc.sipCallNumber = String(callId)
            }
            router.push(controller: vc, animated: true)
        }
    }
    
    func openNewsDescriptionVC(news: NewsEntity) {
        if let vc = R.storyboard.main.newsDescriptionViewController() {
            vc.news = news
            router.push(controller: vc, animated: true)
        }
    }
    
    func openVideoVC(call: VSLCall?) {
        if let vc = R.storyboard.main.videoViewController() {
            vc.activeCall = call
            router.push(controller: vc, animated: true)
        }
    }
}

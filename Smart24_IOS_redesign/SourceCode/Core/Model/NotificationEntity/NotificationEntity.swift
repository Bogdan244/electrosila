//
//  NotificationEntity.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/28/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import Foundation
import ObjectMapper

class NotificationEntity: BaseEntity {
    
    dynamic var title           = ""
    dynamic var descrip         = ""
    dynamic var publish_date    = ""
    dynamic var readStatus      = false
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        uid             <- map[MapperKey.notification_id]
        title           <- map[MapperKey.title]
        descrip         <- map[MapperKey.description]
        publish_date    <- map[MapperKey.publish_date]
    }
    
    override class func objectForMapping(map: Map) -> BaseMappable? {
        return NotificationEntity()
    }
}
